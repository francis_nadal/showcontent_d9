<?php
 
namespace Drupal\showcontent_article_form;
 
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
 
class InjectParagraph {
 
  public static function addParagraphSubmit(array $form, FormStateInterface $form_state)
  {

    
    // \Drupal::logger('addParagraphSubmit')->notice('<pre><code>' . print_r("testing ra", 1) . '</code></pre>');
    // Set the paragraph field in question.
    $paragraphFieldName = 'field_image_library_paragraphs';
 
    // Extract the paragraph field from the form.
    $element = NestedArray::getValue($form, [$paragraphFieldName, 'widget']);
    \Drupal::logger('$element')->notice('<pre><code>' . print_r(array_keys($element), 1) . '</code></pre>');
    $field_name = $element['#field_name'];
    $field_parents = $element['#field_parents'];
    
 
    // Get the widget state.
    $widget_state = static::getWidgetState($field_parents, $field_name, $form_state);

    // \Drupal::logger('$element_attributes')->notice('<pre><code>' . print_r($element['#attributes'], 1) . '</code></pre>');

    // \Drupal::logger('$form_state')->notice('<pre><code>' . print_r($form_state, 1) . '</code></pre>');

    // \Drupal::logger('$gettype')->notice('<pre><code>' . print_r(gettype($widget_state), 1) . '</code></pre>');

    // \Drupal::logger('$widget_state')->notice('<pre><code>' . print_r(array_keys($widget_state), 1) . '</code></pre>');
    
    // \Drupal::logger('$selected_bundle')->notice('<pre><code>' . print_r($widget_state['selected_bundle'], 1) . '</code></pre>');
 
    // Inject the new paragraph and increment the items count.
    // $widget_state['selected_bundle']= 'image_library_paragraphs';
    // \Drupal::logger('$selected_bundle')->notice('<pre><code>' . print_r($widget_state['selected_bundle'][], 1) . '</code></pre>');
    
    // $tempstore = \Drupal::service('user.private_tempstore')->get('showcontent_article_form');
    // $the_counter = $tempstore->get('image_counter');
    // $somedata = $_SESSION['showcontent_article_form']['image_counter'];

    $request = \Drupal::request();
    $session = $request->getSession();
    $image_counter = $session->get('showcontent_article_form');

    \Drupal::logger('$the_counter')->notice('<pre><code>' . print_r($image_counter, 1) . '</code></pre>');

    for($i = 1; $i <= $image_counter; $i++) {
      $widget_state['items_count'] = $i;
      // \Drupal::logger('$widget_state_items_count]')->notice('<pre><code>' . print_r($widget_state['items_count'], 1) . '</code></pre>');
  
      // Update the widget state.
      static::setWidgetState($field_parents, $field_name, $form_state, $widget_state);
  
      // Rebuild the form.
      $form_state->setRebuild();
    }

    $session->remove('showcontent_article_form');
    
  }
 
  public static function addParagraphAjax(array $form, FormStateInterface $form_state) //
  {
    // \Drupal::logger('addParagraphAjax')->notice('<pre><code>' . print_r("testing ra", 1) . '</code></pre>');
    // Set the paragraph field in question.
    $paragraphFieldName = 'field_image_library_paragraphs';
 
    // Extract the paragraph field from the form.
    $element = NestedArray::getValue($form, [$paragraphFieldName, 'widget']);
 
    // Update the field with the needed Ajax formatting.
    $delta = $element['#max_delta'];
    $element[$delta]['#prefix'] = '<div class="ajax-new-content">' . (isset($element[$delta]['#prefix']) ? $element[$delta]['#prefix'] : '');
    $element[$delta]['#suffix'] = (isset($element[$delta]['#suffix']) ? $element[$delta]['#suffix'] : '') . '</div>';
 
    // Clear the add more delta from the paragraph module.
    NestedArray::setValue(
      $element,
      ['add_more', 'add_more_delta', '#value'],
      ''
    );
 
    // Return the paragraph element.
    return $element;
  }
 
  public static function getWidgetState(array $parents, $field_name, FormStateInterface $form_state) {
    // \Drupal::logger('getWidgetState')->notice('<pre><code>' . print_r("testing ra", 1) . '</code></pre>');
    return NestedArray::getValue($form_state->getStorage(), array_merge(['field_storage', '#parents'], $parents, ['#fields', $field_name]));
  }
 
  public static function setWidgetState(array $parents, $field_name, FormStateInterface $form_state, array $field_state) {
    // \Drupal::logger('setWidgetState')->notice('<pre><code>' . print_r("testing ra", 1) . '</code></pre>');
    NestedArray::setValue($form_state->getStorage(), array_merge(['field_storage', '#parents'], $parents, ['#fields', $field_name]), $field_state);
  }
}